//
//  Model.swift
//  Top10Footballers
//
//  Created by Loci Olah on 28.10.2021.
//

import Foundation

struct Player {
    var firstName: String
    var height: String
    var lastName: String
    var nationality: String
    var teamName: String
    var photoPlayer: String
    var logoClub: String
}
