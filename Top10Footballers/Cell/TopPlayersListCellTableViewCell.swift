//
//  TopPlayersListCellTableViewCell.swift
//  Top10Footballers
//
//  Created by Loci Olah on 28.10.2021.
//

import UIKit

    class Players10ListTableViewCell: UITableViewCell {
        
        @IBOutlet weak var imagePlayer: UIImageView!
        @IBOutlet weak var logoClubImage: UIImageView!
        @IBOutlet weak var firstNameLabel: UILabel!
        @IBOutlet weak var heightLabel: UILabel!
        @IBOutlet weak var lastNameLabel: UILabel!
        @IBOutlet weak var nationalityLabel: UILabel!
        
        @IBOutlet weak var teamNameLabel: UILabel!
        
        override func awakeFromNib() {
            super.awakeFromNib()
            
        }
        
        func update(_ player: Player) {
            firstNameLabel.text = player.firstName
            heightLabel.text = player.height
            lastNameLabel.text = player.lastName
            nationalityLabel.text = player.nationality
            teamNameLabel.text = player.teamName
            getImage(urlImage: player.photoPlayer, photo: imagePlayer)
            getImage(urlImage: player.logoClub, photo: logoClubImage)
        }
        
        func getImage(urlImage: String, photo: UIImageView) {
            
            if let url = URL(string: urlImage) {
                let task = URLSession.shared.dataTask(with: url) { data, response, error in
                    guard let data = data, error == nil else { return }
                    
                    DispatchQueue.main.async {
                        photo.image = UIImage(data: data)
                    }
                }
                task.resume()
            }
        }
        
    }
